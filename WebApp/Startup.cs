﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Domain.Commands;
using Domain.Model;
using Domain.Queries;
using Domain.Repositories;
using Domain.Services.Abstractions;
using Domain.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApp.Modules;

namespace WebApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            // Add framework services.
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ClientContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("Domain")));
            services.AddMvc()
                .AddJsonOptions(option => option.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss");
            services.AddRouting();

            var builder  = new ContainerBuilder();

            builder
                .RegisterGeneric(typeof(DbRepository<>))
                .As(typeof(IRepository<>))
                .SingleInstance();

            builder
                .RegisterType<ClientContext>()
                .As<ClientContext>()
                .SingleInstance();

            builder
                .RegisterType<BillService>()
                .As<IBillService>();

            builder
                .RegisterType<ClientService>()
                .As<IClientService>();

            builder
                .RegisterType<DisplayNumberService>()
                .As<IDisplayNumberService>();

            builder
                .RegisterGeneric(typeof(IdService<>))
                .As(typeof(IIdService<>));

            builder
                .RegisterType<InnVerifierService>()
                .As<IInnVerifierService>();

            builder
                .RegisterType<NumberOfBillService>()
                .As<INumberForBillService>();

            builder
                .RegisterAssemblyTypes(typeof(GetBillsQuery).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IQuery<,>));

            builder
                .RegisterGeneric(typeof(QueryFor<>))
                .As(typeof(IQueryFor<>));

            builder
                .RegisterTypedFactory<IQueryBuilder>()
                .InstancePerLifetimeScope();

            builder
                .RegisterTypedFactory<IQueryFactory>()
                .InstancePerLifetimeScope();

            builder
                .RegisterAssemblyTypes(typeof(CreateBillCommand).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(ICommand<>));

            builder
                .RegisterType<CommandBuilder>()
                .As<ICommandBuilder>()
                .InstancePerLifetimeScope();

            builder
                .RegisterTypedFactory<ICommandFactory>()
                .InstancePerLifetimeScope();

            builder.Populate(services);

            this.ApplicationContainer = builder.Build();

            return ApplicationContainer.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, 
            ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(policy => policy.AllowAnyOrigin());

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}");
                routes.MapRoute(
                    name: "deafult1",
                    template: "{controller}/{id}/{action?}");
            });

            applicationLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
        }
    }
}
