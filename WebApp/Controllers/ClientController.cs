﻿using System;
using System.Collections.Generic;
using Domain.Commands;
using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Queries;
using Domain.Queries.Criteries;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{

    [Route("[controller]")]
    public class ClientController : BaseController
    {
        public ClientController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder) 
            : base(queryBuilder, commandBuilder) {}

        [HttpPost("add")]
        public void Add(string name, string inn)
        {
            CommandBuilder.Execute(new CreateClientContext
            {
                Name = name,
                Inn = inn
            });
        }

        [HttpPut("{id}/ChangeName")]
        public void ChangeName(int id, string name)
        {
            CommandBuilder.Execute(new ChangeClientNameCommandContext
            {
                Id = id,
                Name = name
            });
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CommandBuilder.Execute(new DeleteClientCommandContext{Id = id});
        }

        [Route("/Clients")]
        [HttpGet]
        public JsonResult GetMany(int offset, int count)
        {
            return Json(QueryBuilder.For<IEnumerable<Client>>()
                .With(new OffsetCountCriterion
                {
                    Count = count,
                    Offset = offset
                }));
        }

        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            return Json(QueryBuilder.For<Client>()
                .With(new ClientCriterion {Id = id}));
        }

        [HttpGet("{id}/bills")]
        public JsonResult Bills(int id, int offset, int count)
        {
            return Json(QueryBuilder.For<IEnumerable<Bill>>().With(new ClientBillsCriterion
            {
                ClientId = id,
                Offset = offset,
                Count = count
            }));
        }
    }
}