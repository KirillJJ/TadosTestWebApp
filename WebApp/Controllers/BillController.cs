﻿using System.Collections.Generic;
using Domain.Commands;
using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Queries;
using Domain.Queries.Criteries;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Route("[controller]")]
    public class BillController : BaseController
    {
        public BillController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
            : base(queryBuilder, commandBuilder)
        {
        }

        [HttpPost("add")]
        public void Add(int clientId, decimal summ)
        {
            CommandBuilder.Execute(new CreateBillCommandContext
            {
                ClientId = clientId,
                Sum = summ
            });
        }

        [HttpPut("{id}/pay")]
        public IActionResult Pay(int id)
        {
            CommandBuilder.Execute(new PayForBillCommandContext{Id = id});
            return new EmptyResult();
        }

        [HttpGet("/Bills")]
        public JsonResult Bills(int offset, int count)
        {
            return Json(QueryBuilder.For<IEnumerable<Bill>>().With(new OffsetCountCriterion
            {
                Offset = offset,
                Count = count
            }));
        }
    }
}