﻿using System;
using System.Collections.Generic;
using Domain.Commands;
using Domain.Entities;
using Domain.Queries;
using Domain.Queries.Criteries;
using Domain.Queries.ResponseEnteties;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Route("[controller]")]
    public class StatsController : BaseController
    {
        public StatsController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
              : base(queryBuilder, commandBuilder)
        {
        }
        
        [HttpGet("payedBillsSum")]
        public JsonResult PayedBillsSum(
            DateTime startDateTime, DateTime endDateTime, int count)
        {
            return Json(QueryBuilder.For<IEnumerable<PayedBillsStatsResponse>>().With(new PayedBillsSumStatsCriterion
            {
                StartDateTime = startDateTime,
                EndDateTime = endDateTime,
                Count = count
            }));
        }


        [HttpGet("Client/{id}/Bills")]
        public JsonResult GetClientBillsSum(int id, DateTime startDateTime, DateTime endDateTime)
        {
            return Json(QueryBuilder
                .For<BillsStatsResponse>()
                .With(new ClientBillsStatsCriterion
                {
                    ClientId = id,
                    StartDateTime = startDateTime,
                    EndDateTime = endDateTime
                }));
        }

        [HttpGet("Bills")]
        public JsonResult Bills()
        {
            return Json(QueryBuilder.For<BillsStatsResponse>().With(new BillsStatsCriterion()));
        }
    }
}