﻿using Domain.Commands;
using Domain.Queries;
using Microsoft.AspNetCore.Mvc;
using WebApp.Utils;

namespace WebApp.Controllers
{
    [CustomExceptionFilter]
    public abstract class BaseController : Controller
    {
        protected readonly ICommandBuilder CommandBuilder;
        protected readonly IQueryBuilder QueryBuilder;

        protected BaseController(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            QueryBuilder = queryBuilder;
            CommandBuilder = commandBuilder;
        }
    }
}