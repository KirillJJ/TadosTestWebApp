﻿using System;

namespace Domain.Entities
{
    public class Bill : IEntity
    {
        public int Id { get; protected set; }

        public decimal Summ { get; protected set; }

        public int ClientId { get; protected set; }

        public int Number { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime PayedAt { get; protected set; }

        public string DisplayNumber { get; protected set; }

        public bool Paid { get; protected set; }

        public Bill(decimal summ, int clientId, int number, DateTime createdAt, string displayNumber)
        {
            if (clientId < 0)
                throw new ArgumentOutOfRangeException(nameof(clientId), "Id must be positive");

            if (summ <= 0)
                throw new ArgumentOutOfRangeException(nameof(summ), "Summ must be more than 0");

            if (createdAt > DateTime.UtcNow)
                throw new ArgumentOutOfRangeException(nameof(createdAt), "Creation date can't be in future");

            if (string.IsNullOrWhiteSpace(displayNumber))
                throw new ArgumentNullException(nameof(displayNumber));

            ClientId = clientId;
            Summ = summ;
            Number = number;
            CreatedAt = createdAt;
            DisplayNumber = displayNumber;
        }

        public Bill()
        {
            
        }
        public void Pay(DateTime dateOfPay)
        {
            Paid = true;
            PayedAt = dateOfPay;
        }
    }
}
