﻿using System;

namespace Domain.Services.Abstractions
{
    public interface IBillService
    {
        void Add(decimal summ, int clientId);

        void Pay(int billId);

        void Delete(int billId);
    }
}
