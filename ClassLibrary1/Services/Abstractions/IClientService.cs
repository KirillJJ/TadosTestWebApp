﻿namespace Domain.Services.Abstractions
{
    public interface IClientService
    {
        void Add(string name, string innNum);

        void ChangeName(int id, string name);

        void Delete(int id);
    }
}
