﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Services.Abstractions
{
    public interface IDisplayNumberService
    {
        string GiveDisplayNumber(DateTime creatineDate, int num);
    }
}
