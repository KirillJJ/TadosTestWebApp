﻿using Domain.Entities;

namespace Domain.Services.Abstractions
{
    public interface INumberForBillService
    {
        int GiveValidNumberForBill();
    }
}
