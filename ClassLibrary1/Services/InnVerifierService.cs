﻿using System;
using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class InnVerifierService : IInnVerifierService
    {
        private static readonly int INN_LENGTH_1 = 10;
        private static readonly int INN_LENGTH_2 = 12;

        private readonly IRepository<Client> _repository;

        public InnVerifierService(IRepository<Client> repository)
        {
            _repository = repository;
        }

        public bool IsValid(string inn)
        {
            if (inn == null)
                throw new ArgumentNullException(nameof(inn));
            int v;
            return (inn.Length == INN_LENGTH_1 || inn.Length == INN_LENGTH_2) && Int32.TryParse(inn, out v);
        }

        public bool IsFree(string inn)
        {
            if (!IsValid(inn))
                throw new ArgumentException("inn is invalid");

            return _repository.All().All(v => !string.Equals(v.InnNum, inn));
        }
    }
}
