﻿using System;
using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class BillService : IBillService
    {

        private readonly IRepository<Bill> _billRepository;
        private readonly IRepository<Client> _clientRepository;
        private readonly INumberForBillService _numberForBillService;
        private readonly IDisplayNumberService _displayNumberService;

        public BillService(IRepository<Bill> repository, INumberForBillService numberForBillService, 
            IDisplayNumberService displayNumberService, IRepository<Client> clientRepository)
        {
            _billRepository = repository;
            _numberForBillService = numberForBillService;
            _displayNumberService = displayNumberService;
            _clientRepository = clientRepository;
        }

        public void Add(decimal summ, int clientId)
        {
            if (summ <= 0)
                throw new InvalidOperationException("Summ can't be <= 0");

            if (clientId <= 0)
                throw new InvalidOperationException("ClientID can't be < 0");

            if (_clientRepository.All().All(v => v.Id != clientId))
                throw new InvalidOperationException("No client with such id");

            int num = _numberForBillService.GiveValidNumberForBill();

            DateTime creationDate = DateTime.UtcNow;
            _billRepository.Add(new Bill(summ, clientId, num, creationDate,
                _displayNumberService.GiveDisplayNumber(creationDate, num)));

        }

        public void Pay(int billId)
        {
            Bill bill = _billRepository.All().FirstOrDefault(v => v.Id == billId);

            if (bill == null)
                throw new ArgumentException("Have no bill with this ID");

            bill.Pay(DateTime.UtcNow);
        }

        public void Delete(int billId)
        {
            Bill bill = _billRepository.All().FirstOrDefault(v => v.Id == billId);

            if (bill == null)
                throw new ArgumentException("Have no bill with this ID");

            _billRepository.Delete(billId);
        }
    }
}
