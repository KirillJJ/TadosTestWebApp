﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class DisplayNumberService : IDisplayNumberService
    {
        public string GiveDisplayNumber(DateTime creatineDate, int num)
        {
            return $"{creatineDate.Month}.{creatineDate.Year}-{num}";
        }
    }
}
