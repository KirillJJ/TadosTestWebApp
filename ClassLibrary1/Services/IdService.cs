﻿using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class IdService<TEntity> : IIdService<TEntity> where TEntity : IEntity
    {
        private readonly IRepository<TEntity> _repository;

        public IdService(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public int GiveFreeId()
        {
            if (!_repository.All().Any())
                return 0;
            return _repository.All().Max(v=>v.Id) + 1;
        }
    }
}