﻿using System;
using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Services
{
    public class NumberOfBillService : INumberForBillService
    {
        public NumberOfBillService(IRepository<Bill> repository)
        {
            _repository = repository;
        }

        private readonly IRepository<Bill> _repository;

        public int GiveValidNumberForBill()
        {
            if (!_repository
                .All()
                .Any(v => v.CreatedAt.Month == DateTime.UtcNow.Month && v.CreatedAt.Year == DateTime.UtcNow.Year))
                return 0;

            return _repository.All()
                .Where(v => v.CreatedAt.Month == DateTime.UtcNow.Month && v.CreatedAt.Year == DateTime.UtcNow.Year)
                .Max(v => v.Number) + 1;
        }
    }
}
