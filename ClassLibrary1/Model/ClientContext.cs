﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.Model
{
    public class ClientContext : DbContext //TODO где это должно быть?
    {
        public DbSet<Bill> Bills { get; set; }
        public DbSet<Client> Clients { get; set; }



        public ClientContext(DbContextOptions options) : base(options)
        {
        }
    }
}