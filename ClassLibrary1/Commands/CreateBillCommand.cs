﻿using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Services.Abstractions;

namespace Domain.Commands
{
    public class CreateBillCommand : ICommand<CreateBillCommandContext>
    {
        private readonly IBillService _service;

        public CreateBillCommand(IBillService service)
        {
            _service = service;
        }

        public void Execute(CreateBillCommandContext commandContext)
        {
            _service.Add(commandContext.Sum, commandContext.ClientId);
        }
    }
}