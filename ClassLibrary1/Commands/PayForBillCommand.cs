﻿using Domain.Commands.Contexts;
using Domain.Services.Abstractions;

namespace Domain.Commands
{
    public class PayForBillCommand : ICommand<PayForBillCommandContext>
    {
        private readonly IBillService _service;

        public PayForBillCommand(IBillService service)
        {
            _service = service;
        }

        public void Execute(PayForBillCommandContext commandContext)
        {
            _service.Pay(commandContext.Id);
        }
    }
}