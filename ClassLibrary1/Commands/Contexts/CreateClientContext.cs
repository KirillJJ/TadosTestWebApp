﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Commands.CommandContext;

namespace Domain.Commands.Contexts
{
    public class CreateClientContext : ICommandContext
    {
        public string Name;
        public string Inn;
    }
}
