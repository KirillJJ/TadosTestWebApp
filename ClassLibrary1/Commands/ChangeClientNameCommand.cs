﻿using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Services.Abstractions;

namespace Domain.Commands
{
    public class ChangeClientNameCommand : ICommand<ChangeClientNameCommandContext>
    {
        private readonly IClientService _service;

        public ChangeClientNameCommand(IClientService service)
        {
            _service = service;
        }

        public void Execute(ChangeClientNameCommandContext commandContext)
        {
            _service.ChangeName(commandContext.Id, commandContext.Name);
        }
    }
}