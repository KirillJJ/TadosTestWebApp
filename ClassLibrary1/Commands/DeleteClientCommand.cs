﻿using System.Linq;
using Domain.Commands.CommandContext;
using Domain.Commands.Contexts;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services.Abstractions;

namespace Domain.Commands
{
    public class DeleteClientCommand : ICommand<DeleteClientCommandContext>
    {
        private readonly IClientService _service;

        public DeleteClientCommand(IClientService service)
        {
            _service = service;
        }

        public void Execute(DeleteClientCommandContext commandContext)
        {
            _service.Delete(commandContext.Id);
        }
    }
}