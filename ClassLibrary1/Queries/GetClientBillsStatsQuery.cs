﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Queries.ResponseEnteties;
using Domain.Repositories;

namespace Domain.Queries
{
    public class GetClientBillsStatsQuery 
        : IQuery<ClientBillsStatsCriterion, BillsStatsResponse>
    {

        private readonly IRepository<Bill> _repository;

        public GetClientBillsStatsQuery(IRepository<Bill> repository)
        {
            _repository = repository;
        }

        public BillsStatsResponse Ask(ClientBillsStatsCriterion criterion)
        {

            var allClientBills = _repository.All()
                .Where(v => v.CreatedAt >= criterion.StartDateTime)
                .Where(v => criterion.EndDateTime == DateTime.MinValue || v.CreatedAt <= criterion.EndDateTime)
                .Where(v => v.ClientId == criterion.ClientId)
                .ToList();

            return new BillsStatsResponse
            {
                TotalCount = allClientBills.Count,

                PayedCount = allClientBills.Count(v => v.Paid),

                UnpayedCount = allClientBills.Count(v => !v.Paid),

                TotalSum = allClientBills.Sum(v => v.Summ),

                PayedSum = allClientBills
                    .Where(v => v.Paid)
                    .Sum(v => v.Summ),

                UnpayedSum = allClientBills
                    .Where(v => !v.Paid)
                    .Sum(v => v.Summ)
            };
        }
    }
}