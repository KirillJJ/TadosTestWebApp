﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Queries.Criteria;

namespace Domain.Queries.Criteries
{
    public class OffsetCountCriterion : ICriterion
    {
        public int Offset;
        public int Count;
    }
}
