﻿using System;
using Domain.Queries.Criteria;

namespace Domain.Queries.Criteries
{
    public class ClientBillsStatsCriterion : ICriterion
    {
        public int ClientId;
        public DateTime StartDateTime;
        public DateTime EndDateTime;
    }
}