﻿using Domain.Queries.Criteria;

namespace Domain.Queries.Criteries
{
    public class ClientBillsCriterion : ICriterion
    {
        public int ClientId;
        public int Offset;
        public int Count;
    }
}