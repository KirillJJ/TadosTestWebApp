﻿using System;
using System.Threading;
using Domain.Queries.Criteria;

namespace Domain.Queries.Criteries
{
    public class PayedBillsSumStatsCriterion : ICriterion
    {
        public DateTime StartDateTime;
        public DateTime EndDateTime;
        public int Count;

    }
}