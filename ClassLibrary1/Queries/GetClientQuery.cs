﻿using System.Linq;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Repositories;

namespace Domain.Queries
{
    public class GetClientQuery : IQuery<ClientCriterion, Client>
    {
        private readonly IRepository<Client> _repository;

        public GetClientQuery(IRepository<Client> repository)
        {
            _repository = repository;
        }

        public Client Ask(ClientCriterion criterion)
        {
            return _repository.All().FirstOrDefault(v => v.Id == criterion.Id);
        }
    }
}