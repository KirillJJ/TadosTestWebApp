﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Queries.Criteries;
using Domain.Queries.ResponseEnteties;
using Domain.Repositories;

namespace Domain.Queries
{
    public class GetPayedBillsSumStatsQuery : IQuery<PayedBillsSumStatsCriterion, IEnumerable<PayedBillsStatsResponse>>
    {
        private readonly IRepository<Bill> _billRepository;
        private readonly IRepository<Client> _clientRepository;

        public GetPayedBillsSumStatsQuery(IRepository<Bill> repository, IRepository<Client> clientRepository)
        {
            _billRepository = repository;
            _clientRepository = clientRepository;
        }

        public IEnumerable<PayedBillsStatsResponse> Ask(PayedBillsSumStatsCriterion criterion)
        { 
            int count = criterion.Count;
            if (count < 0) return Enumerable.Empty<PayedBillsStatsResponse>();
            else if (count > 100) count = 100;

            return _billRepository.All()
                .Where(v => v.Paid)
                .Where(v => v.PayedAt >= criterion.StartDateTime)
                .Where(v => v.PayedAt <= criterion.EndDateTime || criterion.EndDateTime == DateTime.MinValue)
                .GroupBy(v => v.ClientId)
                .Select(v => new {ClientId = v.Key, Summ = v.Sum(x => x.Summ)})
                .OrderByDescending(v => v.Summ)
                .Take(count)
                .Select(v => new PayedBillsStatsResponse
                {
                    Client = _clientRepository.All().First(c => c.Id == v.ClientId),
                    Sum = v.Summ
                });
        }
    }
}