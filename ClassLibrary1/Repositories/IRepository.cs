﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(int id);

        void Save();

        IEnumerable<TEntity> All();
    }
}
