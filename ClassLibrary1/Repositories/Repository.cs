﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>  where TEntity : IEntity
    {
        private readonly List<TEntity> _list = new List<TEntity>();



        public void Add(TEntity entity)
        {
            _list.Add(entity);
        }

        public IEnumerable<TEntity> All()
        {
            return _list;
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
